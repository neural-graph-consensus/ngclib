# Test ngcdir is done using ngc_ensemble as example, after training.
from pathlib import Path
from ngclib.graph_cfg import GraphCfg
from ngclib.ngcdir import NGCDir
from ngclib.ngcdir.ngcdir_status import _iteration_model_status, _iteration_data_status
import yaml

cwd = Path(__file__).absolute().parent


def test_ngc_dir_properties():
    graph_cfg = GraphCfg(yaml.safe_load(open(cwd / "ngc_ensemble.yaml", "r"))["graph"])
    ngcdir = NGCDir(Path(__file__).parent / "ngc_ensemble", graph_cfg)
    assert ngcdir.num_iterations == 3
    assert ngcdir.nodes == {
        "rgb": {"type": "RGB", "input_node": True},
        "hsv": {"type": "HSV", "input_node": True},
        "depth": {"type": "Depth", "input_node": False},
        "semantic": {"type": "Semantic", "input_node": False},
    }
    assert ngcdir.edges == [
        "Single Link rgb -> depth",
        "Single Link rgb -> semantic",
        "Single Link hsv -> depth",
        "Ensemble semantic -> depth",
        "Ensemble depth -> semantic",
    ]
    assert ngcdir.num_trained_edges(iteration=1) == 5
    assert ngcdir.num_trained_edges(iteration=2) == 5
    assert ngcdir.num_trained_edges(iteration=3) == 0


def test_ngcdir_data_status():
    graph_cfg = GraphCfg(yaml.safe_load(open(cwd / "ngc_ensemble.yaml", "r"))["graph"])
    ngcdir = NGCDir(Path(__file__).parent / "ngc_ensemble", graph_cfg)
    data_status = [_iteration_data_status(ngcdir, i + 1) for i in range(ngcdir.num_iterations)]
    assert data_status[0] == {
        "semantic": {"supervised": 80, "semisupervised": 0},
        "rgb": {"supervised": 80, "semisupervised": 0},
        "hsv": {"supervised": 80, "semisupervised": 0},
        "depth": {"supervised": 80, "semisupervised": 0},
    }
    assert data_status[1] == {
        "semantic": {"supervised": 110, "semisupervised": 20},
        "rgb": {"supervised": 130, "semisupervised": 0},
        "hsv": {"supervised": 130, "semisupervised": 0},
        "depth": {"supervised": 110, "semisupervised": 20},
    }
    assert data_status[2] == {
        "semantic": {"supervised": 30, "semisupervised": 20},
        "rgb": {"supervised": 50, "semisupervised": 0},
        "hsv": {"supervised": 50, "semisupervised": 0},
        "depth": {"supervised": 30, "semisupervised": 20},
    }


def test_ngcdir_model_status():
    graph_cfg = GraphCfg(yaml.safe_load(open(cwd / "ngc_ensemble.yaml", "r"))["graph"])
    ngcdir = NGCDir(Path(__file__).parent / "ngc_ensemble", graph_cfg)
    model_status = [_iteration_model_status(ngcdir, i + 1) for i in range(ngcdir.num_iterations)]
    assert [_m["trained_edges"] for _m in model_status] == [5, 5, 0]
    for i, model_iteration_status in enumerate(model_status):
        if i == 2:
            assert len(model_iteration_status["edges"]) == 0
        for edge in model_iteration_status["edges"]:
            assert edge["train"]["status"] == "finished"


if __name__ == "__main__":
    test_ngcdir_model_status()
    # print(NGCDir(Path(__file__).parent / "ngc_ensemble", Path(__file__).parent / "ngc_ensemble.yaml"))
