"""Dronescapes Nodes importer. Simplifies importing the dronescapes nodes into ngclib"""
from typing import List, Dict
from ngclib.graph_cfg import NGCNodesImporter

from .rgb import RGB
from .hsv import HSV
from .depth import Depth
from .semantic import Semantic
from .edges import Edges
from .normals import Normals
from .soft_segmentation import SoftSegmentation
from .soft_edges import SoftEdges


class DronescapesNodesImporter(NGCNodesImporter):
    """Dronescapes Nodes importer. Simplifies importing the dronescapes nodes into ngclib"""

    _available_nodes = {
        "RGB": RGB,
        "HSV": HSV,
        "Depth": Depth,
        "Semantic": Semantic,
        "Edges": Edges,
        "Normals": Normals,
        "SoftSegmentation": SoftSegmentation,
        "SoftEdges": SoftEdges
    }

    def __init__(self, node_names: List[str], node_types: List[str], node_args: Dict[str, Dict]):
        super().__init__(node_names, node_types, node_args, node_str_to_type=DronescapesNodesImporter._available_nodes)
