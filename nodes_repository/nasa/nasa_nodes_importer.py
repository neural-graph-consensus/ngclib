"""NASA Nodes importer. Simplifies importing the NASA nodes into ngclib"""
# pylint: disable=unused-wildcard-import, wildcard-import
from typing import List, Dict
from ngclib.graph_cfg import NGCNodesImporter

from .nasa_nodes import *


class NASANodesImporter(NGCNodesImporter):
    """NASA Nodes importer. Simplifies importing the NASA nodes into ngclib"""
    _available_nodes = {
        "AerosolOpticalDepth": AerosolOpticalDepth,
        "CarbonMonoxide": CarbonMonoxide,
        "Chlorophyll": Chlorophyll,
        "CloudFraction": CloudFraction,
        "CloudOpticalThickness": CloudOpticalThickness,
        "CloudParticleRadius": CloudParticleRadius,
        "CloudWaterContent": CloudWaterContent,
        "Fire": Fire,
        "LeafAreaIndex": LeafAreaIndex,
        "NitrogenDioxide": NitrogenDioxide,
        "Ozone": Ozone,
        "SeaSurfaceTemperature": SeaSurfaceTemperature,
        "SnowCover": SnowCover,
        "Temperature": Temperature,
        "TemperatureAnomaly": TemperatureAnomaly,
        "Vegetation": Vegetation,
        "WaterVapor": WaterVapor,
    }

    def __init__(self, node_names: List[str], node_types: List[str], node_args: Dict[str, Dict]):
        super().__init__(node_names, node_types, node_args, node_str_to_type=NASANodesImporter._available_nodes)
