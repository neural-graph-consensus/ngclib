"""Types module for the NGC library."""
# pylint: disable=pointless-string-statement
from typing import Callable, List, Dict, Union
import torch as tr

# agg: VxD -> D
VoteFn = Callable[[tr.Tensor], tr.Tensor]
# f(int, int) => nn.Module. Used to construct a single edge provided (in_dims, out_dims) as params.
NGCEdgeFn = Callable[[int, int], tr.nn.Module]
# {edge: NGCEdgeFn} or just NGCEdgeFn. Used to construct an entire NGC
NGCModelFns = Union[Dict[str, NGCEdgeFn], NGCEdgeFn]

"""
ConfigEdges is a mapping like this:
edges:
  SL:
    - [a, b]
    - [c, d]
  ENS:
    - [a, b, c]
"""
ConfigEdges = Dict[str, List[List[str]]]
