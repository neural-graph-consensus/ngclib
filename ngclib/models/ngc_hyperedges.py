"""NGC HyperEdges module"""
from __future__ import annotations
from typing import Dict, List
from overrides import overrides
import torch as tr
from nwgraph import Message
from nwgraph.edge import Edge, IdentityEdge
from nwgraph.node import CatNode
from .ngc import NGC, NGCNode, ConfigEdges, VoteFn
from .ngc_edge import NGCEdge
from .edges import SingleLink, EnsembleEdge, HyperEdge
from .ngc_ensemble import NGCEnsemble
from ..utils import NGCModelFns

class NGCHyperEdges(NGC):
    """NGC HyperEdges module"""
    def __init__(self, edges: List[NGCEdge], vote_fn: VoteFn, input_node_names: List[str]):
        super().__init__(edges, vote_fn, input_node_names)
        self._num_iterations = None
        self._validate_edges()

    @overrides(check_signature=False)
    def aggregate(self, messages: Dict[NGCNode, List[Message]], t: int) -> Dict[NGCEdge, tr.Tensor]:
        # TODO: why isn't this using t at all ?
        # TODO: why isn't this using vote_fn at all?
        res = {}
        for node, node_messages in messages.items():
            if len(node_messages) == 0:
                continue
            if isinstance(node, CatNode):
                # properly reorder messages so they are stacked as the CatNode was created (usually sorted).
                if sum(x.content.shape[-1] for x in node_messages) != node.dims[-1]:
                    continue
                msg_nodes = [self.name_to_edge[node_message.source].input_node for node_message in node_messages]
                in_nodes_ix = [msg_nodes.index(n) for n in node.nodes]
                reordered_messages = [node_messages[ix] for ix in in_nodes_ix]
                messages_content = [msg.content for msg in reordered_messages]

                cat_node_data = node.make_cat(messages_content)
                assert cat_node_data.shape[-1] == node.dims[-1], (messages_content, node)
                res[node] = cat_node_data
            else:
                # TODO for other messages. For example a H2 should only propagate H1's messages
                mean_messages = tr.stack([message.content for message in messages[node]], dim=0).mean(dim=0)
                res[node] = mean_messages
        return res

    @overrides(check_signature=False)
    def update(self, aggregation: Dict[NGCNode, tr.Tensor], t: int) -> Dict[NGCNode, tr.Tensor]:
        return aggregation

    @property
    @overrides
    def num_iterations(self) -> int:
        return self._num_iterations

    @overrides(check_signature=False)
    def subgraph(self, edges) -> NGCHyperEdges:
        this_edges = [self.name_to_edge[edge.name] for edge in edges]
        subgraph_input_nodes = set()
        for edge in edges:
            for node in edge.nodes:
                if node in self.input_nodes:
                    subgraph_input_nodes.add(node.name)
        new_graph = NGCHyperEdges(this_edges, self.vote_fn, subgraph_input_nodes)
        return new_graph

    @staticmethod
    @overrides
    def edge_name_from_cfg_str(edge_type: str, cfg_edge_name: List[str], node_names: List[str],
                               input_node_names: List[str]) -> str:
        assert edge_type in ("SL", "ENS", "H1", "H2", "H2R"), edge_type
        assert isinstance(node_names[0], str), node_names
        if edge_type in ("SL", "ENS"):
            return NGCEnsemble.edge_name_from_cfg_str(edge_type, cfg_edge_name, node_names, input_node_names)

        if edge_type == "H1":
            first_part = input_node_names if cfg_edge_name[0] == "*" else cfg_edge_name[0]
            assert isinstance(first_part, list), first_part
            for node_name in first_part:
                assert node_name in input_node_names, cfg_edge_name
            assert cfg_edge_name[1] not in input_node_names and cfg_edge_name[1] in node_names, cfg_edge_name
            name = ",".join(first_part)
            return f"H1 [{name}] -> {cfg_edge_name[1]}"

        if edge_type == "H2":
            assert False, "TODO"

        if edge_type == "H2R":
            first_part = node_names if cfg_edge_name[0] == "*" else cfg_edge_name[0]
            assert isinstance(first_part, list), first_part
            for node_name in first_part:
                assert node_name in node_names, cfg_edge_name
            assert cfg_edge_name[1] not in input_node_names and cfg_edge_name[1] in node_names, cfg_edge_name
            assert cfg_edge_name[1] in first_part, f"H2R must have the output node in first part too: {cfg_edge_name}"
            name = ",".join(first_part)
            return f"H2R [{name}] -> {cfg_edge_name[1]}"

        return None

    @staticmethod
    @overrides
    def build_graph_from_edges_cfg(nodes: List[NGCNode], edges_cfg: ConfigEdges, ngc_edge_fn: NGCModelFns,
                                   vote_fn: VoteFn, input_node_names: List[str], *args, **kwargs) -> NGCHyperEdges:
        edges = NGCHyperEdges.build_edges(nodes, edges_cfg, ngc_edge_fn, input_node_names)
        return NGCHyperEdges(edges, vote_fn, input_node_names)

    @staticmethod
    def build_edges(nodes: List[NGCNode], edges_cfg: ConfigEdges, ngc_edge_fn: NGCModelFns,
                    input_node_names: List[str]) -> List[NGCEdge]:
        """Just build the edges and return them. Used in tests"""
        assert len(edges_cfg) > 0
        name_to_node = {node.name: node for node in nodes}
        h1s = edges_cfg["H1"] if "H1" in edges_cfg else []
        h2rs = edges_cfg["H2R"] if "H2R" in edges_cfg else []
        cat_nodes: Dict[tuple, CatNode] = {}

        # first, start with regular SL/TH edges from NGCEnsemble
        ens_edges = NGCEnsemble.build_edges_from_graph_cfg(nodes, edges_cfg, ngc_edge_fn, input_node_names)
        edges: List[Edge] = [*ens_edges]

        # instantiate H1 edges
        for str_edge in h1s:
            edge_name = NGCHyperEdges.edge_name_from_cfg_str("H1", str_edge, list(name_to_node), input_node_names)
            edge_fn = ngc_edge_fn[edge_name] if isinstance(ngc_edge_fn, dict) else ngc_edge_fn
            first_part = input_node_names if str_edge[0] == "*" else str_edge[0]
            input_nodes = tuple(name_to_node[n] for n in first_part)
            output_node = name_to_node[str_edge[1]]
            for input_node in input_nodes:
                assert input_node in input_node_names
            assert output_node not in input_nodes

            if input_nodes not in cat_nodes:
                cat_nodes[input_nodes] = CatNode(input_nodes)
            cat_node = cat_nodes[input_nodes]
            id_edges = [IdentityEdge(i, cat_node, name=f"Copy {i} -> {cat_node}") for i in input_nodes]
            out_edge = HyperEdge(cat_node, output_node, edge_name, edge_fn, hyper_edge_type="H1")
            edges = [*edges, *id_edges, out_edge]

        # TODO: instantiate H2 edges

        # instantiate H2R edges
        for str_edge in h2rs:
            edge_name = NGCHyperEdges.edge_name_from_cfg_str("H2R", str_edge, list(name_to_node), input_node_names)
            edge_fn = ngc_edge_fn[edge_name] if isinstance(ngc_edge_fn, dict) else ngc_edge_fn
            first_part = [n.name for n in nodes] if str_edge[0] == "*" else str_edge[0]
            # In1, In2
            input_nodes = tuple(name_to_node[n] for n in first_part if n in input_node_names)
            # Out1, Out2, Out3
            h1_output_nodes = tuple(name_to_node[n] for n in first_part if n not in input_node_names)
            # Out1 only
            output_node = name_to_node[str_edge[1]]
            assert output_node in h1_output_nodes, f"H2R must have a residual connection {h1_output_nodes}"
            assert input_nodes in cat_nodes, f"{input_nodes} not in {cat_nodes}"
            # CatNode([in1, in2, in3]) = cat_in
            cat_input_node = cat_nodes[input_nodes]
            # check all h1s are there as well: H1(cat_in, out1), H1(cat_in, out2) and H1(cat_in, out3)
            h1s = [e for e in edges if isinstance(e, HyperEdge) and e.hyper_edge_type == "H1"]
            for node in h1_output_nodes:
                h1_edges = [h1 for h1 in h1s if h1.output_node == node]
                assert len(h1_edges) == 1, (node, h1s)
                h1_edge = h1s[0]
                assert h1_edge.input_node == cat_input_node
            # Create CatNode([cat_in, out1, out2, out3]) = cat_out
            if tuple([cat_input_node, *h1_output_nodes]) not in cat_nodes:
                cat_nodes[tuple([cat_input_node, *h1_output_nodes])] = CatNode([cat_input_node, *h1_output_nodes])

            cat_out = cat_nodes[tuple([cat_input_node, *h1_output_nodes])]
            # Create IdentityEdge(out1, cat_out), IdentityEdge(out2, cat_out), IdentityEdge(out3, cat_out)
            id_edges = [IdentityEdge(o, cat_out, name=f"Copy {o} -> {cat_out}") for o in h1_output_nodes]
            # Create IdentityEdge(cat_input_node, cat_out)
            id_edges = [*id_edges, IdentityEdge(cat_input_node, cat_out, name=f"Copy {cat_input_node} -> {cat_out}")]
            out_edge = HyperEdge(cat_out, output_node, edge_name, edge_fn, hyper_edge_type="H2R")
            edges = [*edges, *id_edges, out_edge]
        return edges

    def _validate_edges(self):
        min_num_iterations = 0
        for edge in self.edges:
            assert isinstance(edge, (SingleLink, EnsembleEdge, HyperEdge, IdentityEdge)), f"{edge} => {type(edge)}"
            if isinstance(edge, SingleLink):
                min_num_iterations = max(min_num_iterations, 1)
                assert edge.input_node in self.input_nodes and edge.output_node in self.output_nodes, edge

            if isinstance(edge, EnsembleEdge):
                assert edge.input_node in self.output_nodes and edge.output_node in self.output_nodes, edge

            if isinstance(edge, HyperEdge):
                assert isinstance(edge.input_node, CatNode)
                assert edge.output_node in self.output_nodes, edge
                if edge.name.startswith("H1"):
                    min_num_iterations = max(min_num_iterations, 2)
                    for node in edge.input_node.nodes:
                        assert node in self.input_nodes, edge
                elif edge.name.startswith("H2"):
                    min_num_iterations = max(min_num_iterations, 4)
                    self._num_iterations = 4 if self._num_iterations is None else self._num_iterations
        self._num_iterations = min_num_iterations
