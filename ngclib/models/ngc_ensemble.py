"""NGCEnsemble module"""
from __future__ import annotations
from typing import Dict, List
from nwgraph import Message
from overrides import overrides
import torch as tr

from .ngc import NGC, NGCNode
from .ngc_edge import NGCEdge
from .edges import EnsembleEdge, SingleLink
from ..utils import ConfigEdges, VoteFn, NGCModelFns

class NGCEnsemble(NGC):
    """NGCEnsemble implementation"""
    def __init__(self, edges: List[NGCEdge], vote_fn: VoteFn, input_node_names: List[str]):
        super().__init__(edges, vote_fn, input_node_names)
        self._num_iterations: int = None

    @property
    def num_iterations(self) -> int:
        if self._num_iterations is None:
            self._num_iterations = 1
            if len([e for e in self.edges if isinstance(e, EnsembleEdge)]) > 0:
                self._num_iterations = 2
        return self._num_iterations

    @staticmethod
    @overrides
    def edge_name_from_cfg_str(edge_type: str, cfg_edge_name: List[str], node_names: List[str],
                               input_node_names: List[str]) -> str:
        assert edge_type in ("SL", "ENS"), edge_type
        edge_type = "Single Link" if edge_type == "SL" else "Ensemble"
        name = f"{edge_type} {cfg_edge_name[0]} -> {cfg_edge_name[1]}"
        return name

    @staticmethod
    @overrides
    def build_graph_from_edges_cfg(nodes: List[NGCNode], edges_cfg: ConfigEdges, ngc_edge_fn: NGCModelFns,
                                   vote_fn: VoteFn, input_node_names: List[str], *args, **kwargs) -> NGCEnsemble:
        """Builds NGC-Ensemble edges: single links and ensemble edges"""
        edges = NGCEnsemble.build_edges_from_graph_cfg(nodes, edges_cfg, ngc_edge_fn, input_node_names)
        return NGCEnsemble(edges, vote_fn, input_node_names)

    @staticmethod
    def build_edges_from_graph_cfg(nodes: List[NGCNode], edges_cfg: ConfigEdges, ngc_edge_fn: NGCModelFns,
                                   input_node_names: List[str]) -> List[NGCEdge]:
        """Only builds the edges. Used in tests and NGCHyperEdges as well (to avoid code duplication for SL/TH)"""
        sls = edges_cfg["SL"] if "SL" in edges_cfg else []
        enss = edges_cfg["ENS"] if "ENS" in edges_cfg else []
        name_to_node = {node.name: node for node in nodes}

        edges: List[NGCEdge] = []
        for str_edge in sls:
            edge_name = NGCEnsemble.edge_name_from_cfg_str("SL", str_edge, nodes, input_node_names)
            edge_nodes = [name_to_node[n] for n in str_edge]
            edge_fn = ngc_edge_fn[edge_name] if isinstance(ngc_edge_fn, dict) else ngc_edge_fn
            assert len(edge_nodes) == 2, str_edge
            assert edge_nodes[0] in input_node_names and edge_nodes[1] not in input_node_names, edge_nodes
            edge = SingleLink(edge_nodes[0], edge_nodes[1], edge_name, edge_fn)
            edges.append(edge)

        for str_edge in enss:
            edge_name = NGCEnsemble.edge_name_from_cfg_str("ENS", str_edge, nodes, input_node_names)
            edge_nodes = [name_to_node[n] for n in str_edge]
            edge_fn = ngc_edge_fn[edge_name] if isinstance(ngc_edge_fn, dict) else ngc_edge_fn
            assert len(edge_nodes) == 2, str_edge
            assert edge_nodes[0] not in input_node_names and edge_nodes[1] not in input_node_names, edge_nodes
            edge = EnsembleEdge(edge_nodes[0], edge_nodes[1], edge_name, edge_fn)
            edges.append(edge)
        return edges

    @overrides(check_signature=False)
    def aggregate(self, messages: Dict[NGCNode, List[Message]], t: int) -> Dict[NGCEdge, tr.Tensor]:
        assert t < 2, t
        res = {}
        for node, node_messages in messages.items():
            x = tr.stack([x.content for x in node_messages], dim=1)
            if t == 0:
                aggregated = x.median(dim=1)[0]
            else:
                aggregated = self.vote_fn(x, [x.source for x in node_messages])
            res[node] = aggregated
        return res

    @overrides(check_signature=False)
    def subgraph(self, edges: List[NGCEdge]) -> NGC:
        this_edges = [self.name_to_edge[edge.name] for edge in edges]
        subgraph_input_nodes = set()
        for edge in edges:
            for node in edge.nodes:
                if node in self.input_nodes:
                    subgraph_input_nodes.add(node.name)
        new_graph = type(self)(this_edges, self.vote_fn, subgraph_input_nodes)
        return new_graph
