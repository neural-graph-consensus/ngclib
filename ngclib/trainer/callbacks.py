"""Callbacks for an TrainableNGC"""
from pathlib import Path
from pytorch_lightning import Callback, Trainer
from lightning_module_enhanced import LME
from nwgraph import Graph

class PlotGraph(Callback):
    """Plots the graph at fit/test start"""
    def on_fit_start(self, trainer: Trainer, pl_module: LME) -> None:
        assert isinstance(pl_module, LME) and isinstance(pl_module.base_model.base_graph, Graph)
        in_dir = Path(pl_module.logger.log_dir)
        pl_module.base_model.base_graph.to_graphviz().render(in_dir / "graph", format="png", cleanup=True)

    def on_test_start(self, trainer: Trainer, pl_module: LME) -> None:
        assert isinstance(pl_module, LME) and isinstance(pl_module.base_model.base_graph, Graph)
        in_dir = Path(pl_module.logger.log_dir)
        pl_module.base_model.base_graph.to_graphviz().render(in_dir / "graph", format="png", cleanup=True)
