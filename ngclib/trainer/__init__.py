"""Trainer init file"""
from .trainable_ngc import TrainableNGC
from .ngc_trainer import NGCTrainer
