"""Sequential iteration async trainer module"""
import torch as tr
from ...models import NGCEdge

def sequential_async_train(ngc_trainer: "NGCAsyncTrainer"):
    """sequential strategy implementation of NGCAsyncTrainer"""
    device = tr.device("cuda") if tr.cuda.is_available() else tr.device("cpu")
    while not ngc_trainer.are_all_edges_trained():
        edge: NGCEdge
        for edge in ngc_trainer.untrained_edges:
            edge.to(device)
            ngc_trainer.train_one_edge(edge)
