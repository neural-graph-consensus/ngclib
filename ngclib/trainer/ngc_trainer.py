"""NGC Iteration trainer module"""
from pathlib import Path
from abc import ABC, abstractmethod
from typing import Dict, List
from lightning_module_enhanced import LME

from .utils import is_edge_trained
from ..models import NGC, NGCEdge
from ..readers import NGCNpzReader
from ..logger import logger

class NGCTrainer(ABC):
    """
    NGCTrainer interface. Handles ONE iteration only (concept of iteration is for semi-supervised tasks, not NGC)

    Parameters:
        graph The full ngc graph
        train_cfg The config file used to train the NGC. Uses LME semantics.
        train_reader The train reader
        validation_reader The validation reader
        iter_dir The ngcdir/iterX/models directory where this iteration's edges are stored
        dataloader_params The parameters passed to the dataloader (no collate_fn, generator or worker_init)
        trainer_params The parameters passed to the lighting trainer
        seed The seed used for the trainer
    """
    def __init__(self,
                 graph: NGC,
                 train_cfg: Dict,
                 train_reader: NGCNpzReader,
                 validation_reader: NGCNpzReader,
                 iter_dir: Path,
                 dataloader_params: Dict,
                 trainer_params: Dict,
                 seed: int = 0,
                 ):
        assert iter_dir.name == "models", f"Iter dir is not called 'models', probably a mistake ({iter_dir})"
        if validation_reader is None:
            logger.warning("No validation set provided. Using train set as validation set as well.")
            validation_reader = train_reader
        assert not "collate_fn" in dataloader_params, "'collate_fn' is provided by the NGC Reader directly."
        assert not "generator" in dataloader_params, "'generator' is controlled using the seed parameter"
        assert not "worker_init" in dataloader_params, "'worker_init' is set by the trainer directly"
        assert sorted(train_reader.nodes, key=lambda n: n.name) == \
               sorted(validation_reader.nodes, key=lambda n: n.name), (train_reader.nodes, validation_reader.nodes)

        self.graph = graph.to("cpu")
        self.train_cfg = train_cfg
        self.train_reader = train_reader
        self.validation_reader = validation_reader
        self.iter_dir = iter_dir
        self.dataloader_params = dataloader_params
        self.trainer_params = trainer_params
        self.seed = seed
        LME(graph).reset_parameters(seed)

        logger.info(f"Building all the subgraphs for this graph ({len(graph.edges)} edges)")
        self.subgraphs: Dict[str, NGC] = self.graph.edge_subgraphs(graph.input_node_names, graph.num_iterations)

    @abstractmethod
    def run(self):
        """The method to run this iteration. Must be overriden by parallelism stragies"""

    # Properties

    @property
    def trained_edges(self) -> List[NGCEdge]:
        """Returns a list of all trained edges"""
        return [edge for edge in self.graph.edges if is_edge_trained(edge, self.iter_dir)]

    @property
    def untrained_edges(self) -> List[NGCEdge]:
        """Returns a list of all untrained edges"""
        untrained = [edge for edge in self.graph.edges if not is_edge_trained(edge, self.iter_dir)]
        return untrained

    # Public methods

    def get_dependencies(self, edge: NGCEdge) -> List[NGCEdge]:
        """Returns a list of all the dependencies of the given edge"""
        res = []
        subgraph_edge: NGCEdge
        for subgraph_edge in self.subgraphs[edge.name].edges:
            if edge.name != subgraph_edge.name:
                res.append(subgraph_edge)
        return res

    def are_dependencies_trained(self, edge: NGCEdge) -> bool:
        """Checks if all edge dependencies are trained for a particular edge via the subgraph"""
        dependency_edges = self.get_dependencies(edge)
        for dependency_edge in dependency_edges:
            if not is_edge_trained(dependency_edge, self.iter_dir):
                return False
        return True

    def are_all_edges_trained(self) -> bool:
        """Checks if all edges are trained"""
        return len(self.untrained_edges) == 0

    def __call__(self):
        return self.run()
