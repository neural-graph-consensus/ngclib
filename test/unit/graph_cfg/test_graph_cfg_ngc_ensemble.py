import yaml
from ngclib.graph_cfg import GraphCfg
from ngclib.models import NGC, NGCV1, NGCEnsemble, NGCHyperEdges
from pathlib import Path

cwd = Path(__file__).absolute().parent

def test_graph_cfg_ngc_ensemble_ngc_type():
    graph_cfg = GraphCfg(yaml.safe_load(open(cwd / "test_graph_ensemble.yaml", "r")))
    assert graph_cfg.ngc_type == NGCEnsemble


def test_graph_cfg_ngc_ensemble_properties_1():
    graph_cfg = GraphCfg(yaml.safe_load(open(cwd / "test_graph_ensemble.yaml", "r")))
    assert graph_cfg.edges_raw == {
        "SL": [["rgb", "semantic"], ["rgb", "depth"]],
        "ENS": [["depth", "semantic"], ["semantic", "depth"]],
    }
    assert graph_cfg.edges == [
        "Single Link rgb -> semantic",
        "Single Link rgb -> depth",
        "Ensemble depth -> semantic",
        "Ensemble semantic -> depth",
    ]
