import torch as tr
from torch import nn
from ngclib.models import NGCEnsemble, NGCNode
from ngclib.models.edges import SingleLink, EnsembleEdge

def test_ngc_ensemble_message_pass_1():
    MB = 10
    nodes = {
        "a": NGCNode("a", (1, )), "b": NGCNode("b", (2, )), "c": NGCNode("c", (3, )), "d": NGCNode("d", (4, ))
    }
    sl1 = SingleLink(nodes["a"], nodes["c"], None, nn.Linear)
    sl2 = SingleLink(nodes["b"], nodes["c"], None, nn.Linear)
    ens = EnsembleEdge(nodes["c"], nodes["d"], None, nn.Linear)
    vote_fn = lambda votes, sources: votes.mean(axis=1)
    graph = NGCEnsemble([sl1, sl2, ens], vote_fn, ["a", "b"])
    x = {"a": tr.randn(MB, 1), "b": tr.randn(MB, 2)}
    y = graph.forward(x)
    assert tr.allclose(y["a"], x["a"])
    assert tr.allclose(y["b"], x["b"])
    assert y["c"].shape == (MB, 3)
    assert y["d"].shape == (MB, 4)
