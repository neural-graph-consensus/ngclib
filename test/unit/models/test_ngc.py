from torch import nn
from ngclib.models import NGC, NGCNode
from ngclib.models.edges import SingleLink

def test_ngc_shrink_nodes():
    nodes = {"a": NGCNode("a", (1, )), "b": NGCNode("b", (2, )), "c": NGCNode("c", (3, )), "d": NGCNode("d", (4, ))}
    sl = SingleLink(nodes["a"], nodes["b"], None, nn.Linear)
    sl2 = SingleLink(nodes["b"], nodes["d"], None, nn.Linear)
    ngc = NGC([sl, sl2], lambda votes, sources: votes[0], input_node_names=["a", "c"])
    assert ngc.input_nodes == ["a"]
