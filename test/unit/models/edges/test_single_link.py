import torch as tr
from torch import nn
from ngclib.models import NGCNode
from ngclib.models.edges import SingleLink
from lightning_module_enhanced import LME


def test_single_link_1():
    sl = SingleLink(NGCNode("a", (1, )), NGCNode("b", (2, )), None, nn.Linear)
    assert not sl is None
    assert LME(sl).num_params == 4


def test_forward_1():
    sl = SingleLink(NGCNode("a", (10, )), NGCNode("b", (20, )), None, nn.Linear)
    MB = tr.randint(1, 10, (1,))
    sl_input = tr.randn(MB, 10)
    y = sl.forward(sl_input)
    assert y.shape == (MB, 20)


def test_message_pass_1():
    sl = SingleLink(NGCNode("a", (10, )), NGCNode("b", (20, )), None, nn.Linear)
    MB = tr.randint(1, 10, (1,))
    sl_input = tr.randn(MB, 10)
    sl.message_pass(sl_input, 0)
    assert len(sl.output_node.messages) == 1
    assert sl.output_node.messages[0].content.shape == (MB, 20)


def test_message_pass_2():
    sl = SingleLink(NGCNode("a", (10, )), NGCNode("b", (20, )), None, nn.Linear)
    MB = tr.randint(1, 10, (1,))
    sl_input = tr.randn(MB, 10)
    sl.message_pass(sl_input, 0)
    sl.message_pass(sl_input, 0)
    assert len(sl.output_node.messages) == 1
    assert sl.output_node.messages[0].content.shape == (MB, 20)


def test_message_pass_3():
    sl = SingleLink(NGCNode("a", (10, )), NGCNode("b", (20, )), None, nn.Linear)
    MB = tr.randint(1, 10, (1,))
    sl.message_pass(tr.randn(MB, 10), 0)
    sl.message_pass(tr.randn(2 * MB, 10), 0)
    assert len(sl.output_node.messages) == 2
    shapes = [sl.output_node.messages[0].content.shape, sl.output_node.messages[1].content.shape]
    assert sorted(shapes) == [(MB, 20), (2 * MB, 20)]
