import tempfile
import torch as tr
from typing import Dict, List
from pathlib import Path
from ngclib.models import NGC, NGCNode, NGCHyperEdges
from ngclib.graph_cfg import NGCNodesImporter
import numpy as np
from torch import nn
from ngclib.trainer.async_trainer import NGCAsyncTrainer
from torch.utils.data import Dataset

class InNode(NGCNode): pass
class OutNode(NGCNode): pass

class _Dataset(Dataset):
    def __init__(self, nodes: List[NGCNode], x: Dict[str, tr.Tensor], gt: Dict[str, tr.Tensor]):
        self.x = x
        self.gt = gt
        self.nodes = nodes
        self.in_nodes = [node for node in nodes if node in x.keys()]
        self.out_nodes = [node for node in nodes if node in gt.keys()]

    def __getitem__(self, ix: int):
        x_keys = np.random.permutation(list(self.x.keys()))
        gt_keys = np.random.permutation(list(self.gt.keys()))
        return {
            "data": {k: self.x[k][ix] for k in x_keys},
            "labels": {k: self.gt[k][ix] for k in gt_keys}
        }

    def __len__(self):
        return len(self.x[list(self.x.keys())[0]])

    def subreader(self, nodes):
        x = {k: v for k, v in self.x.items() if k in nodes}
        gt = {k: v for k, v in self.gt.items() if k in nodes}
        return _Dataset(nodes, x, gt)

    def collate_fn(self, batch: List):
        data = {node.name: tr.stack([y["data"][node] for y in batch]) for node in self.in_nodes}
        labels = {node.name: tr.stack([y["labels"][node] for y in batch]) for node in self.out_nodes}
        return {"data": data, "labels": labels}

def get_nodes():
    nodes = {"in1": 1, "in2": 2, "in3": 5, "in4": 10, "in5": 3, "out1": 10}
    input_node_names = ["in1", "in2", "in3", "in4", "in5"]

    nodes = NGCNodesImporter(node_names=list(nodes.keys()),
                             node_types=["InNode" if node in input_node_names else "OutNode" for node in nodes.keys()],
                             node_args={node_name: {"dims": [v]} for node_name, v in nodes.items()},
                             node_str_to_type={"InNode": InNode, "OutNode": OutNode},
                            ).nodes
    vote_fn = lambda votes, sources: votes.mean(axis=1)
    model_fn = nn.Linear
    return nodes, vote_fn, model_fn, input_node_names

def get_data(ngc: NGC, n_data: int) -> _Dataset:
    x = {node.name: tr.randn(n_data, *node.dims) for node in ngc.input_nodes}
    gt = {node.name: tr.randn(n_data, *node.dims) for node in ngc.output_nodes}
    return _Dataset(ngc.nodes, x, gt)

def test_train_one_edge_hyperedges():
    nodes, vote_fn, model_fn, input_node_names = get_nodes()
    tmp_weights_dir = tempfile.TemporaryDirectory()
    weights_dir = Path(tmp_weights_dir.name)
    edges_str = {
        "H1": [
            ["*", "out1"],
        ]
    }
    ngc = NGCHyperEdges.build_graph_from_edges_cfg(nodes, edges_str, model_fn, vote_fn, input_node_names)
    train_reader = get_data(ngc, 80)
    val_reader = get_data(ngc, 20)
    train_cfg = {"optimizer": {"type": "adam", "args": {"lr": 0.01}}}
    for edge in ngc.edges:
        edge.output_node.criterion_fn = lambda y, gt: (y - gt).abs().mean()
    trainer = NGCAsyncTrainer(ngc, train_cfg=train_cfg,
                              train_reader=train_reader, validation_reader=val_reader,
                              iter_dir=weights_dir / "models", dataloader_params={},
                              trainer_params={"max_epochs": 3},
                              strategy="sequential")
    trainer.train_one_edge(ngc.edges[-1])

    # evaluate correctness
    xs = val_reader.collate_fn([val_reader[i] for i in range(len(val_reader))])["data"]
    ngc.load_all_weights(weights_dir / "models")
    y1 = ngc.forward(xs)["out1"]

    # new model with random weights
    ngc2 = NGCHyperEdges.build_graph_from_edges_cfg(nodes, edges_str, model_fn, vote_fn, input_node_names)
    y2 = ngc2.forward(xs)["out1"]
    assert not tr.allclose(y1, y2)

    # load pretrained model from first part of the test
    ngc2.load_all_weights(weights_dir / "models")
    y3 = ngc2.forward(xs)["out1"]
    assert tr.allclose(y1, y3)
    tmp_weights_dir.cleanup()

if __name__ == "__main__":
    test_train_one_edge_hyperedges()
